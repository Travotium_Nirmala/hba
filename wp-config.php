<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hba');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'X0ONh7n$I)J;k[6YL=~v1ZTt8G+<3-A_wIN:YH`q,|]!z=v;j~WFSH.oaTaN]hdV');
define('SECURE_AUTH_KEY',  'WNHCrG[M 2< m{p$rV{<)TPS+VGg&8`rOk4|esUYl@hM3.N`CM GO|m0lY6],~#}');
define('LOGGED_IN_KEY',    'L&Se5,WDp(=X_uaQJq%wql2OF?q67q+EHua!5=VG2!?3*9pzc:achhm,0Z+f+}+(');
define('NONCE_KEY',        'Z2kE,xrw2^X@0R8Fr@d ;(c #]1P6*m23tIO^!xb#Ol~8M0b)S4t5qs}Nw/s67o+');
define('AUTH_SALT',        '*8K^m)3uvc$MCH+Gn`xY*8w[ h[inyJf_I`*#L.Z(eRbL #uOo|b&xnACq?ca3y2');
define('SECURE_AUTH_SALT', '5W.D^&8D<3Po.Q8gRJ=Z8j_r:kjQv#f^*cCnKWq=$~5Aedc`H^J?Yw<Y`*g(vF=D');
define('LOGGED_IN_SALT',   'RATV(gPBp#T>R{DXxVziZ4@,UzI_Wz5#G]`+{cPr^Toi{{[!8lKVwEwENFR(w,|P');
define('NONCE_SALT',       'Fr!;dg,8O;?EapG?po09Q]u3 +Xc?evkAxZmz>Mn#T6^zbTNR.rZ mNt+%}^{EN[');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'hba_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
